from gym.envs.registration import register


register(
    id='MBRLCartpole-v0',
    entry_point='baselines.env.cartpole:CartpoleEnv'
)

register(
    id='MBRLCartpole-v1',
    entry_point='baselines.env.cartpole:CartPoleSwingUpEnv'
)
